#!/usr/bin/python
'''

SA Cancer Genomic Facility
Created on Jul 18, 2012

TODO: Merge this into htSeq-count

@author: David Lawrence
'''

from argparse import ArgumentParser

def handleArgs():
    parser = ArgumentParser(description='Fasta Grep')
    parser.add_argument('--gff',        required=True,                      help="GFF containing miRNA coordinates")
    parser.add_argument('--out',        required=True,                      help="Output CSV")
    parser.add_argument('--type',       required=True,                      help="GFF feature type")
    parser.add_argument('--index',      default="ID",                       help="GFF index attribute")
    parser.add_argument('--extraColumn',                                    help="Add extra GFF attribute to CSV")
    parser.add_argument('countFiles',   metavar='countFiles',   nargs="+",  help='HTSeq count output files (tab separated)')
    return parser.parse_args()

def main(args):
    gff = {}
    for (id, feature) in gffToHash(args.gff, args.index).iteritems():
        if feature.type == args.type:
            gff[id] = feature

    countFiles = {}
    for fileName in args.countFiles:
        name = nameFromFileName(fileName)
        countFiles[name] = fileToHash(file(fileName), '\t')

    names = sorted(countFiles.keys())
    sortByAttr = args.index
    if args.extraColumn:
        sortByAttr = args.extraColumn
    sortGFFFeatures = lambda x : gff[x].attr[sortByAttr]
    gffIds = sorted(gff.keys(), key=sortGFFFeatures)

    rows = hashToCSVRows(countFiles, args.index, names, gffIds)
    header = [ args.index ]
    if args.extraColumn:
        header += [ args.extraColumn ]
        for r in rows:
            feature = gff[r[args.index]]
            r[args.extraColumn] = feature.attr[args.extraColumn]
    
    header += names
    util.writeCSVDict(args.out, header, rows)
        
        
def gffToHash(gff, indexAttribute):    
    indexedGff = {}
    for feature in HTSeq.GFF_Reader(gff):
        indexedGff[feature.attr[indexAttribute]] = feature
    return indexedGff
        
def mkPath(path):
    if path and not os.path.exists(path):
        os.makedirs(path)

def mkPathForFile(f):
    mkPath(os.path.dirname(f))

def nameFromFileName(filename):
    return os.path.splitext(os.path.basename(filename))[0]


def fileToHash(source, sep=None):
    data = {}
    for line in source:
        line = line.rstrip()
        key = line
        value = None
        if sep:
            items = line.split(sep)
            if len(items) >= 2:
                (key, value) = items[0:2]
        data[key] = value
    return data

def hashToCSVRows(countFiles, idName, columns, rowIds):
    rows = []
    for id in rowIds:
        row = { idName : id }
        for c in columns:
            val = countFiles[c].get(id)
            row[c] = val
        rows.append(row)
    return rows


if __name__ == '__main__':
    args = handleArgs()
    main(args)

